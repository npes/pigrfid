'''
Inspiration from https://console.hivemq.cloud/clients/python-paho/
created by: Nikolaj Simonsen | november 2023
repo:  
'''

from socket import error as socket_error
import paho.mqtt.client as paho
from paho import mqtt
import secrets
from datetime import datetime
from collections import Counter
import json
from logger import simple_logger

log = simple_logger.create_logger('FM-503_readings.log')

client_id = secrets.client_id
hive_mqtt_cluster_url = secrets.hive_mqtt_cluster_url
hive_mqtt_user = secrets.hive_mqtt_user
hive_mqtt_password = secrets.hive_mqtt_password
hive_mqtt_port = int(secrets.hive_mqtt_port)
hive_mqtt_websocket_port = secrets.hive_mqtt_websocket_port
topic = 'rfid_reading'
log.info(f'broker: {hive_mqtt_cluster_url} topic: {topic}')

# setting callbacks for different events to see if it works, print the message etc.
def on_connect(client, userdata, flags, rc, properties=None):
    print("CONNACK received with code %s." % rc)

# print which topic was subscribed to
def on_subscribe(client, userdata, mid, granted_qos, properties=None):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

# print message, useful for checking if it was successful
def on_message(client, userdata, msg):
    messages = json.loads(msg.payload)
    uniques = []
    for message in messages:
        # print(f'{datetime.fromtimestamp(message["time_epoch"])}')
        print(f'time_epoch: {message["time_epoch"]} time_utc: {datetime.utcfromtimestamp(message["time_epoch"])} tag_reading: {message["tag_reading"]} reader_id: {message["reader_id"]}')
        log.info(f'time_epoch: {message["time_epoch"]}, time_utc: {datetime.utcfromtimestamp(message["time_epoch"])}, tag_reading: {message["tag_reading"]}, reader_id: {message["reader_id"]}')
        if len(message["tag_reading"]) == 33:
            uniques.append(message["tag_reading"])
        #print(f'message length: {len(message["tag_reading"])}')
    # count how many times a tag has been seen in reading
    # print(Counter(uniques).keys()) # equals to list(set(words))
    # print(Counter(uniques).values()) # counts the elements' frequency

    uniques = list(dict.fromkeys(uniques))
    print(f'unique tags seen: {len(uniques)}')
    log.info(f'unique tags seen: {len(uniques)}')
    for unique in uniques:
        print(f'unique tags: {unique} tag_Text: {int(unique[21:29],16)}')
        log.info(f'unique tags: {unique} tag_Text: {int(unique[21:29],16)}')
    print('\n')
    
# def on_disconnect(client, userdata, msg):
#     print(f'Disconnected')
    

try: 
    client = paho.Client(client_id=client_id, userdata=None, protocol=paho.MQTTv5)
    client.on_connect = on_connect
    client.tls_set(tls_version=mqtt.client.ssl.PROTOCOL_TLS)
    client.username_pw_set(hive_mqtt_user, hive_mqtt_password)
    print('connecting to broker')
    client.connect(hive_mqtt_cluster_url, hive_mqtt_port)
    print(f'connected to {hive_mqtt_cluster_url} on port {hive_mqtt_port} ')

    # setting callbacks, use separate functions like above for better visibility
    client.on_subscribe = on_subscribe
    client.on_message = on_message
    #client.on_publish = on_publish
    #client.on_disconnect = on_disconnect

    client.subscribe(topic)
    print(f'Subscribed to topics: {topic}')

    client.loop_forever()

except socket_error as e:
    print(f'{e}')
    exit(0)

except Exception as e:
    print(e)
    
except KeyboardInterrupt:
    client.loop_stop()
    client.disconnect()