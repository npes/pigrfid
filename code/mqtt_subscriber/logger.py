class simple_logger():

    def create_logger(filename):
        import logging

        def __init__(self) -> None:
            pass

        #LOG_FORMAT = '%(levelname)s %(asctime)s - %(message)s'
        LOG_FORMAT = '%(message)s'
        logging.basicConfig(filename = filename, level=logging.DEBUG, format=LOG_FORMAT,filemode='a')
        logger = logging.getLogger()
        logger.info('Logging started')
        return logger
