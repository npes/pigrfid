'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 4
'''
# library import
import machine
import utime

# global variables
led_onboard = machine.Pin("LED", machine.Pin.OUT)

# main program
while True:
    led_onboard.value(1)
    utime.sleep(1)
    led_onboard.value(0)
    utime.sleep(1)
