from machine import UART, Pin
import time

# timing
read_delay = 0.1

#configure serial port
uart0 = UART(0, baudrate=38400,  bits=8, parity=None, stop=1, tx=Pin(0), rx=Pin(1))

# FM-503 commands
fw_version = b'\nV\r'
reader_id = b'\nS\r'
query_epc = b'\nQ\r'
multi_epc = b'\nU\r'
TID = b'\nR2,0,6\r'

def write(command):
    rxData = ''
    uart0.write(command)
    time.sleep(0.1)
    while uart0.any() > 5:
        rxData = uart0.readline().decode('utf-8').rstrip()
    if rxData == 'U':
        rxData = 'No tag present'
        return rxData
    else:
        return rxData

print('reader_id: ' + write(reader_id) +  '\nfw_version: ' + write(fw_version))
time.sleep(2)

while True:
    print(write(multi_epc))

