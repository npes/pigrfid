from wifi import Connection
from ntp_time import set_time
#from mqtt import Mqtt
import secrets
from time import sleep, time, gmtime, localtime
from umqtt.simple import MQTTClient
import machine

# Status led
LED = machine.Pin("LED", machine.Pin.OUT)

#FM503
import json
from machine import UART, Pin

# timing
read_delay = 0.1

#configure serial port
uart0 = UART(0, baudrate=38400,  bits=8, parity=None, stop=1, tx=Pin(0), rx=Pin(1))

# FM-503 commands
fw_version = b'\nV\r'
reader_id = b'\nS\r'
query_epc = b'\nQ\r'
multi_epc = b'\nU\r'
TID = b'\nR2,0,6\r'

def write(command):
    rxData = ''
    uart0.write(command)
    sleep(0.1)
    while uart0.any() > 5:
        rxData = uart0.readline().decode('utf-8').rstrip()
    if rxData == 'U':
        rxData = 0
        return rxData
    else:
        return rxData

print('reader_id: ' + write(reader_id) +  '\nfw_version: ' + write(fw_version))
sleep(2)

# start wifi, connect mqtt, sync to ntp server

def mqtt_connect():
    client = MQTTClient(
    client_id=secrets.client_id,
    server=secrets.hive_mqtt_cluster_url,
    port=8883,
    user=secrets.hive_mqtt_user,
    password=secrets.hive_mqtt_password,
    keepalive=7200,
    ssl=True,
    ssl_params={'server_hostname':secrets.hive_mqtt_cluster_url}
    )
    return client

connection = Connection(networks=secrets.networks)
connection.try_connect_multiple_wlan()
print(connection.isconnected())
    
set_time()

mqttclient = mqtt_connect()
mqttclient.connect()
LED.on() 

#MQTT variables
messages = [] #contains messages, empty after send
topic_pub = b'rfid_reading'

while(1):
    try:
        for i in range(10):
            reading = write(multi_epc)
            #reading = 0
            if reading != 0:
                message = {
                    'reader_id': secrets.client_id,
                    'time_epoch': time(),
                    'tag_reading': reading}
                messages.append(message)
            sleep(0.1)
            
        connection.isconnected()
        if len(messages) > 0:
            topic_msg = json.dumps(messages).encode()    
            mqttclient.publish(topic_pub, topic_msg)
            messages = []
        else:
            print('no readings')
        print('UTC time '+ str(gmtime()))
        sleep(1)
    except KeyboardInterrupt:
        connection.disconnect()
        LED.off()
        break
    except OSError:
        LED.off()
        connection.connect()
        set_time()
        mqttclient.connect()
        LED.on()

