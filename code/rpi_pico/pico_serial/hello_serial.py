from machine import UART, Pin
import time

#configure serial port
uart0 = UART(1, baudrate=9600, tx=Pin(0), rx=Pin(1))

txData = b'hello world\n\r'
uart0.write(txData)
time.sleep(0.5)
