from wifi import Connection
from ntp_time import set_time
#from mqtt import Mqtt
import secrets
from time import sleep, time, gmtime, localtime
from umqtt.simple import MQTTClient
import json

#Timestamp

epoch = time()
print(epoch)

#WIFI connection

connection = Connection(ssid=secrets.ssid, password=secrets.password)
connection.connect()

print(connection.isconnected())
set_time()
print('time unix epoch: ' + str(time()))

#MQTT stuff

def gen_msg():
    message = {'reader_id': secrets.client_id, 'time_epoch': time(), 'time_utc': str(gmtime()), 'tag reading': 'U3400301614369C00324002E7C64B73AA'}
    topic_msg = json.dumps(message).encode()
    return topic_msg

topic_pub = b'rfid_reading'


#print(type(json.dumps(message)))
#print(type(json.dumps(message).encode()))

mqttclient = MQTTClient(
    client_id=secrets.client_id,
    server=secrets.hive_mqtt_cluster_url,
    port=8883,
    user=secrets.hive_mqtt_user,
    password=secrets.hive_mqtt_password,
    keepalive=7200,
    ssl=True,
    ssl_params={'server_hostname':secrets.hive_mqtt_cluster_url}
    )

mqttclient.connect()

while(1):
    try:
        connection.isconnected()
        mqttclient.publish(topic_pub, gen_msg())
        print('UTC time '+ str(gmtime()))
        sleep(1)
    except KeyboardInterrupt:
        connection.disconnect()
        break 