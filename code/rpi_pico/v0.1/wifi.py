import network
import socket
from time import sleep
import machine

# Status led
LED = machine.Pin("LED", machine.Pin.OUT)

class Connection:
    def __init__(self, networks):
        self.networks = networks
        self.wlan = network.WLAN(network.STA_IF)
    
    def try_connect_multiple_wlan(self):
    # Attempt to connect to each network in order
        for ssid, psk in self.networks:
            self.wlan.active(True)
            self.wlan.connect(ssid, psk)

            # Wait for the connection to succeed or fail
            for i in range(20):
                if self.wlan.isconnected():
                    print("Connected to", ssid)
                    print(self.wlan.ifconfig())
                    break
                else:
                    LED.on()
                    time.sleep(0.5)
                    LED.off()
                    time.sleep(0.5)

            # If we successfully connected to a network, stop trying
            if self.wlan.isconnected():
                break
    
    def try_connect_single_wlan(self):
        #Connect to WLAN
        self.wlan.active(True)
        self.wlan.connect(self.ssid, self.password)
        while self.wlan.isconnected() == False:
            print('Waiting for connection...')
            LED.on() 
            sleep(1)
            LED.off() 
        print(self.wlan.ifconfig())
        
    def disconnect(self):
        self.wlan.disconnect()
        
    def isconnected(self):
        return self.wlan.isconnected()
