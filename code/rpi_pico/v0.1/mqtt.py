from umqtt.simple import MQTTClient

class Mqtt:
    def __init__(self, client_id, server, port, user, password, server_hostname):
        self.client_id = client_id
        self.server = server
        self.port = port
        self.user = user
        self.password = password
        self.server_hostname = server_hostname

    def connectMQTT(self):
        client = MQTTClient(client_id=self.client_id,
        server=self.server,
        port=0,
        user=self.user,
        password=self.password,
        keepalive=7200,
        ssl=True,
        ssl_params={'server_hostname': self.server_hostname}
        )
        client.connect()
        return client
        

# def connectMQTT():
# client = MQTTClient(client_id=b"kudzai_raspberrypi_picow",
# server=b"8fbadaf843514ef286a2ae29e80b15a0.s1.eu.hivemq.cloud",
# port=0,
# user=b"mydemoclient",
# password=b"passowrd",
# keepalive=7200,
# ssl=True,
# ssl_params={'server_hostname':'8fbadaf843514ef286a2ae29e80b15a0.s1.eu.hivemq.cloud'}
# )
# 
# client.connect()
# return client