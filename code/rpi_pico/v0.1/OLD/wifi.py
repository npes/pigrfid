import network
import socket
from time import sleep
import machine

# Status led
LED = machine.Pin("LED", machine.Pin.OUT)

class Connection:
    def __init__(self, ssid, password):
        self.ssid = ssid
        self.password = password
        self.wlan = network.WLAN(network.STA_IF)
        
    def connect(self):
        #Connect to WLAN
        self.wlan.active(True)
        self.wlan.connect(self.ssid, self.password)
        while self.wlan.isconnected() == False:
            print('Waiting for connection...')
            LED.on() 
            sleep(1)
            LED.off() 
        print(self.wlan.ifconfig())
        
    def disconnect(self):
        self.wlan.disconnect()
        
    def isconnected(self):
        return self.wlan.isconnected()
