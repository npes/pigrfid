from fastapi import FastAPI
from pydantic import BaseModel
from typing import List
from db import init_db, create, read

class Reading(BaseModel):
    reading_id: str
    reading_timestamp: str

reading_store = []

init_db()

app = FastAPI()

cursor = init_db()

@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/reading")
async def create_item(readings: List[Reading]):
    # print(reading.reading_id, reading.reading_timestamp)
    # return {"reading": reading.dict()}
    for item in readings:
        reading_store.append(item)
        create(cursor=cursor, reading_id=item.reading_id, reading_timestamp=item.reading_timestamp)
    # print(reading_store)
    return readings


@app.get("/readings", response_model=List[Reading])
async def fetch_readings():
    items = read(cursor=cursor)
    for item in items:
        # print(type(item[0]))
        item_reading = {'reading_id': '', 'reading_timestamp': ''}
        item_reading["reading_id"] = item[0]
        item_reading["reading_timestamp"] = item[1]
        reading_store.append(item_reading)
        #print(f'reading = {item_reading} readingtype = {type(item_reading)}')
    # print(f'reading_store = {reading_store} readingstore type = {type(reading_store[0])}')
    return reading_store