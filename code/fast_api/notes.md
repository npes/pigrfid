# Notes for pigrfid fastapi

## venv with specific python version

```bash
py --list # list installed python versions
py -3.10 -m venv fastapi_env # use python 3.10 for venv
fastapi_env\Scripts\activate.bat # activate environment 
```

## Install fast api in env

```bash
pip install "fastapi[all]" # includes uvicorn server
```

## Run uvicorn/fastapi

```bash
uvicorn main:app --reload

(fastapi_env) λ uvicorn main:app --reload
INFO:     Will watch for changes in these directories: ['C:\\Users\\localadmin\\Documents\\personal_gitlab\\pigrfid\\code\\fast_api']
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Started reloader process [39536] using WatchFiles
INFO:     Started server process [42328]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
```
Swagger docs at [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)
Redoc docs at [http://127.0.0.1:8000/redoc](http://127.0.0.1:8000/redoc)

## Documentation/guides

- Fastapi webapi guide [https://realpython.com/fastapi-python-web-apis/](https://realpython.com/fastapi-python-web-apis/)
- fastapi documentation [https://fastapi.tiangolo.com/](https://fastapi.tiangolo.com/)