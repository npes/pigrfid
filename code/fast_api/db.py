import sqlite3

def init_db():
    conn = sqlite3.connect('readings.db')
    cur = conn.cursor()

    with conn:
        cur.execute('CREATE TABLE IF NOT EXISTS Readings(reading_id TEXT, reading_timestamp TEXT)')   
    return cur

def create(cursor, reading_id: str, reading_timestamp: str):
    cursor.execute('INSERT INTO Readings(reading_id, reading_timestamp) VALUES (?, ?)', (reading_id, reading_timestamp))    

def read(cursor):
    cursor.execute('SELECT reading_id, reading_timestamp FROM Readings')
    return cursor