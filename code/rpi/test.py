#!/usr/bin/python
from time import sleep
import serial
from datetime import datetime, timezone
#print(datetime.now(timezone.utc))

# FM-503 commands
fw_version = b'\nV\r'
reader_id = b'\nS\r'
query_epc = b'\nQ\r'
multi_epc = b'\nU\r'
TID = b'\nR2,0,6\r'
fw_hex = b'[0x0A 0x56 0x0D]'

# usb device
usbvendor = 0x067b
usbproduct = 0x2303

#import usb.core

#dev = usb.core.find(idVendor=usbvendor, idProduct=usbproduct)
#print(dev)
#dev.detach_kernel_driver(0)
#dev.attach_kernel_driver(0)

def write(command):
    rxData = 0
#    print(f'bytes sent {ser.write(command)}')
    sleep(0.1)
    ser.write(command)
    ser.reset_output_buffer()
    #sleep(0.1)
    rxData = ser.readline().decode('utf-8').rstrip()
#    ser.reset_input_buffer()
#    print(f'rxData length before {len(rxData)}')
    if len(rxData) > 0:
#        print(f'rxData length after {len(rxData)}')
        if rxData == 'U':
            ser.reset_input_buffer()
            rxData = 'No tag present'
            return rxData
        else:
            return rxData
    else:
#        ser.reset_input_buffer()
        rxData = 'No data received'
        return rxData

# Establish the connection
with serial.Serial('/dev/ttyUSB0', baudrate=38400, timeout=1, stopbits=1, bytesize=8) as ser:
    #print('reader_id: ' + write(reader_id))
    #sleep(1)
    #print('\nfw_version: ' + write(fw_version))
    print('TID\n: ' + write(TID))
    print(f'serial status: open = {ser.is_open}, writable = {ser.writable()}')
#    ser.reset_input_buffer()
#    ser.reset_output_buffer()
    while ser.writable():
        try:
            print(f'{str(datetime.now(timezone.utc))} {write(multi_epc)} in_buffer: {ser.in_waiting}')
        except serial.SerialException as e:
            print(f'Serial exception: {e}')
        except serial.SerialTimeoutException as e:
            print(f'Serial timeout exception: {e}')
        except KeyboardInterrupt:
            exit(0)