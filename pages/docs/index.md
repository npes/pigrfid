---
hide:
  - footer
  - toc
---

# Svine RFID
**Udvikling af “Proof Of Concept” for trådløs RFID modtager**

## Projektansvarlig 

Nikolaj Simonsen 

# Indhold

1. [Udvikling af POC](01_development/02_RPi_Pico_W_dev_environment.md)
2. [Projekt styring](02_project_management/01_project_idea.md)
3. [Vidensmobilisering]()

![blokdiagram](50_images/SvineRFID_blokdiagram_0.1.svg)
