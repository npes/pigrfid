---
hide:
  - footer
---

# Issue 03 - RPI Pico W wifi test

## Information

Prøv et eksempel der forbinder Pico til wifi

## Dokumentation

1. Brug [https://picockpit.com/raspberry-pi/run-web-server-to-control-onboard-led-on-raspberry-pi-pico-w/](https://picockpit.com/raspberry-pi/run-web-server-to-control-onboard-led-on-raspberry-pi-pico-w/)
2. lav en fil `index.html` og gem den på picoen
    ```html
    <!DOCTYPE html>
    <html>
        <head>
            <title>Pico W</title>
        </head>
        <body>
            <h1>Pico W</h1>
            <p>Control the onboard LED</p>
            <a href=\"?led=on\"><button>ON</button></a>&nbsp;
            <a href=\"?led=off\"><button>OFF</button></a>
        </body>
    </html>
    ```
3. lav en fil `secrets.py` og gem den på picoen
    ```py
    secrets = {
        'ssid': 'Replace-this-with-your-Wi-Fi-Name',
        'pw': 'Replace-this-with-your-Wi-Fi-Password'
    }
    ```
   4. Lav en fil og kør eksemplet
    ```py
    import rp2
    import network
    import ubinascii
    import machine
    import urequests as requests
    import time
    from secrets import secrets
    import socket

    # Set country to avoid possible errors
    rp2.country('DE')

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    # If you need to disable powersaving mode
    # wlan.config(pm = 0xa11140)

    # See the MAC address in the wireless chip OTP
    mac = ubinascii.hexlify(network.WLAN().config('mac'),':').decode()
    print('mac = ' + mac)

    # Other things to query
    # print(wlan.config('channel'))
    # print(wlan.config('essid'))
    # print(wlan.config('txpower'))

    # Load login data from different file for safety reasons
    ssid = secrets['ssid']
    pw = secrets['pw']

    wlan.connect(ssid, pw)

    # Wait for connection with 10 second timeout
    timeout = 10
    while timeout > 0:
        if wlan.status() < 0 or wlan.status() >= 3:
            break
        timeout -= 1
        print('Waiting for connection...')
        time.sleep(1)

    # Define blinking function for onboard LED to indicate error codes
    def blink_onboard_led(num_blinks):
        led = machine.Pin('LED', machine.Pin.OUT)
        for i in range(num_blinks):
            led.on()
            time.sleep(.2)
            led.off()
            time.sleep(.2)

    # Handle connection error
    # Error meanings
    # 0  Link Down
    # 1  Link Join
    # 2  Link NoIp
    # 3  Link Up
    # -1 Link Fail
    # -2 Link NoNet
    # -3 Link BadAuth

    wlan_status = wlan.status()
    blink_onboard_led(wlan_status)

    if wlan_status != 3:
        raise RuntimeError('Wi-Fi connection failed')
    else:
        print('Connected')
        status = wlan.ifconfig()
        print('ip = ' + status[0])

    # Function to load in html page
    def get_html(html_name):
        with open(html_name, 'r') as file:
            html = file.read()

        return html

    # HTTP server with socket
    addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

    s = socket.socket()
    s.bind(addr)
    s.listen(1)

    print('Listening on', addr)
    led = machine.Pin('LED', machine.Pin.OUT)

    # Listen for connections
    while True:
        try:
            cl, addr = s.accept()
            print('Client connected from', addr)
            r = cl.recv(1024)
            # print(r)

            r = str(r)
            led_on = r.find('?led=on')
            led_off = r.find('?led=off')
            print('led_on = ', led_on)
            print('led_off = ', led_off)
            if led_on > -1:
                print('LED ON')
                led.value(1)

            if led_off > -1:
                print('LED OFF')
                led.value(0)

            response = get_html('index.html')
            cl.send('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
            cl.send(response)
            cl.close()

        except OSError as e:
            cl.close()
            print('Connection closed')

    # Make GET request
    #request = requests.get('http://www.google.com')
    #print(request.content)
    #request.close()
    ```
4. Forbind med browser til den tildelte ip adresse og bekræft siden vises + du kan tænde og slukke for LED på picoen
    Output fra Thonny:
    ```sh
    mac = 28:cd:c1:01:db:a4
    Waiting for connection...
    Waiting for connection...
    Waiting for connection...
    Connected
    ip = 192.168.22.210
    Listening on ('0.0.0.0', 80)
    Client connected from ('192.168.22.29', 2388)
    led_on =  10
    led_off =  364
    LED ON
    LED OFF
    ```

## Links

- officiel pico w dokumentation 
    [https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html#raspberry-pi-pico-w-and-pico-wh](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html#raspberry-pi-pico-w-and-pico-wh)
- pico w wifi guide 
    [https://datasheets.raspberrypi.com/picow/connecting-to-the-internet-with-pico-w.pdf](https://datasheets.raspberrypi.com/picow/connecting-to-the-internet-with-pico-w.pdf)
- eksempel fuld kode 
    [https://github.com/pi3g/pico-w/tree/main/MicroPython/I%20Pico%20W%20LED%20web%20server](https://github.com/pi3g/pico-w/tree/main/MicroPython/I%20Pico%20W%20LED%20web%20server)

