---
hide:
  - footer
---

# Issue 06 - RPi Pico W FM-503 UART communication

## Putty og FTDi kabel

- læsning af kommandoer gik fint
- forståelse af kommandoer over UART
- kunne ikke umiddelbart skrive kommandoer via UART/Putty, gav forkerte line endings skylden (det var det ikke!!)

## RPi Serial og FTDi kabel

- lodning af pin headers
- rpi pico w datablad og UART0
- hul igennem fra putty til RPi Pico med eksempel program
- mislykkedes forsøg på at skrive kommando via UART, ingenting skete?? Både USB og rpi pico UART sad til på samme tid, det kan man ikke. Så jeg fandt en ekstern strømforsyning (12V/1.5A) til at forsyne FM-503. Det gjorde forskellen og jeg kunne nu kommunikere over UART mellem Pico og FM-503

## RPi W applikation til at sende kommandoer og læse svar

- Udvikling tog lidt tid, det var do forholdsvist simpelt og følgende applikation blev konstrueret.

```py
from machine import UART, Pin
import time

# timing
read_delay = 0.1

#configure serial port
uart0 = UART(0, baudrate=38400,  bits=8, parity=None, stop=1, tx=Pin(0), rx=Pin(1))

# FM-503 commands
fw_version = b'\nV\r'
reader_id = b'\nS\r'
query_epc = b'\nQ\r'
multi_epc = b'\nU\r'
TID = b'\nR2,0,6\r'
fw_hex = b'[0x0A 0x56 0x0D]'

def write(command):
    rxData = ''
    uart0.write(command)
    time.sleep(0.1)
    while uart0.any() > 5:
        rxData = uart0.readline().decode('utf-8').rstrip()
    if rxData == 'U':
        rxData = 'No tag present'
        return rxData
    else:
        return rxData

print('reader_id: ' + write(reader_id) +  '\nfw_version: ' + write(fw_version))
time.sleep(2)

while True:
    print(write(multi_epc))
```

Applikationen sender en multi EPC læse kommando (`<LF>U<CR>`) med 100ms interval.

Hvis et tag læses udskrives svar (PC+EPC+CRC16) til konsollen, hvis ikke så udskrives intet.

Test af applicationen med FM-503 RFID læser og Pico W kan ses her: [PicoW FM-503 test video](../50_images/fm-503_test/VID20230124171716.mp4)