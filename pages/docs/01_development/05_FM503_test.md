---
hide:
  - footer
---

# Issue 05 - FM-503 rfid reader test

## Test software og SDK fra kinesisk leverandør

Jeg har skrevet med den kinesiske producent og fået tilsendt driver og SDK.  
Det er gemt i projektets repo på gitlab.

Umiddelbart burde taggets leverandør kunne læses ud fra TID på [https://www.gs1.org/services/tid-decoder](https://www.gs1.org/services/tid-decoder)

Billeder og video af test kan findes her:

* [Setup 1](../50_images/fm-503_test/IMG20230124100150.jpg)
* [Setup 2](../50_images/fm-503_test/IMG20230124100200.jpg)
* [Distance 1](../50_images/fm-503_test/IMG20230124105008.jpg)
* [Distance 2](../50_images/fm-503_test/IMG20230124105032.jpg)
* [1 tag](../50_images/fm-503_test/IMG20230124111548.jpg)
* [2 tags](../50_images/fm-503_test/IMG20230124111556.jpg)
* [Video 1 - 1 tag](../50_images/fm-503_test/VID20230124104705.mp4)
* [Video 2 - 1 tag angles](../50_images/fm-503_test/VID20230124104802.mp4)
* [Video 3 - 2 tags](../50_images/fm-503_test/VID20230124104857.mp4) 


## Installer drivere

Ud fra driver softwaren kan jeg se at usb-ttl converteren er en *prolific PL-2303* chip.  
Kun driver til vista/xp/7 blev leveret og jeg hentede seneste driver (4.0.1) fra [https://www.prolific.com.tw/US/ShowProduct.aspx?p_id=223&pcid=126.](https://www.prolific.com.tw/US/ShowProduct.aspx?p_id=223&pcid=126.)  

Driveren installeres således (fra *PL23XX_CheckChipVersion_ReadMe.txt*):

1. Install PL-2303 Windows Driver Installer v1.8.0 or above. (*PL23XX-M_LogoDriver_Setup_v401_20220225.exe* er anvendt)
2. Plug PL2303 USB Device and go to Device Manager to check COM Port number. 
3. Run PL2303CheckChipVersion tool and set COM Port number. 
4. Click Check button to show PL-2303 chip version. 

Efter installation af driveren kørte jeg *PL23XX_checkChipVersion_v1020.exe* som informerede om chip version *PL-2303 HXD*

![PL23XX_checkChipVersion_v1020.exe](../50_images/fm503_Check_PL-2303_chip_version_v1020.png)

databladet kan findes her [https://cdn-shop.adafruit.com/datasheets/PL2303HX.pdf](https://cdn-shop.adafruit.com/datasheets/PL2303HX.pdf)

## Testsoftware

Softwaren hedder *ReaderUtility.exe* og skal ikke installeres, bare startes.  
*ReaderUtility.exe* har 2 sider, den ene er til at konfigurere læseren, den anden er til at læse tags.

## Test om et tag kan læses

Læseren er konfigureret således:

![](../50_images/fm503_Reader_Utility_Multi_EPC_settings.png)

Forskellige frekvenser blev afprøvet men det virkede ikke til at gøre den store forskel, læg mærke til at Power er sat til 25dBm som er maksimum mulige sendestyrke og er afgørende for læseafstand.

Når et tag udlæses vises følgende data.

PC er ??  
EPC er en teksstreng, de sidste 8 cifre er taggets id i HEX. Jeg har 2 tags til at teste med, det ene har id: 4870144011 som i HEX er 02E7C64B, det andet har id: 4870116215, hex 02E759B7
CRC16 er cyclic redundancy check  
Count er antal gange tagget er læst i pågældende session

|PC|EPC|CRC16|Count|
|:---|:---|:---|:---|
| 3400 |	301614369C00324002E759B7 |	55EC |		1722 |
| 3400 |	301614369C00324002E7C64B |	73AA |		1336 |

Læserens protocol er baseret på ASCII og kommando strukturen er at sende en kommando indkapslet i `<LF><CR>`, f.eks når et tag læses i *Multi EPC* sendes kommandoen `U`, hele kommandoen er `<LF>U<CR>`

Læserens svar er indkapslet i `<LF><CR><LF>`, umiddlebart med kommandoen som en del af svaret, f.eks er svaret på `<LF>U<CR>` uden at et tag læses `<LF>U<CR><LF>`

*ReaderUtility.exe* sender kontinuerligt `<LF>U<CR>` og eksempel på kommunikation uden tag kan ses herunder:

![ReaderUtility.exe uden tag detekteret](../50_images/fm503_Reader_Utility_Multi_EPC_cmd.png)

Som det ses svinger læsehastigheden umiddelbart mellem 30-50ms uden at et tag læses. Det vides ikke umiddelbart hvor der evt. er flaskehalse i denne frekvens og resultaterne giver et meget løst estimat på læsefrekvenser.

Når 1 tag læses, modtages data fra tagget som en del af kommando svaret, umiddelbart øges læse frekvensen til 100ms

![ReaderUtility.exe 1 tag](../50_images/fm503_Reader_Utility_Multi_EPC_1tag.png)

Når 2 tags læses modtages, data fra begge tags som en del af kommando svaret, umiddelbart øges læse frekvensen til mellem 100-150ms.  
Rækkefølgen på hvilket tag der læses divergerer umiddelbart tilfældigt og det vides ikke hvilke(n) faktor(er) der afgørt dette.

![ReaderUtility.exe 1 tag](../50_images/fm503_Reader_Utility_Multi_EPC_2tags.png)


## Test læse afstand

Læse afstands testen er udført med både 1 og 2 tags, tags vendes i forhold til læseren for at se om antennens polaritet har indvirkning på læseafstanden.

Resultaterne er meget divergerende.  

Læse afstanden er på 0-5 centimeters afstand ret konsistent og tags læses kontinuerligt ved max hastighed.  
På 5-10 centimeters afstand er læsning stadig konsistent med enkelte pauser i udlæsning (mindre end max hastighed)
Ud over 10 centimeter er læsning ikke konsistent, det opleves som om at et tag læses kontinuerligt efter første læsning, der kan spekuleres i om der efter første energi induktion i det passive tag
skal mindre energi fra læseren til at læse et tag?

## Dokumentation af resultater og erfaring

Resultaterne er ens uanset om 1 eller 2 tags anvendes.  
Det virker til at taggets vinkel ifht. læseren har en indflydelse hvor tagget umiddelbart læses bedst vinkelret på læseren.

Når et tag hurtigt føres forbi læseren med en afstand på 10-20 cm læses det næsten hver gang (ca. 9 ud af 10 gange). 

Læseren er testet uden materiale foran antenne, en hurtig test med et stykke plastic over antennen reducerer læse afstanden til ca. 5 centimeter.
Det kan have betydning ifht. antennens placering i grisestien og hvilket chassis enheden indbygges i. 

## Register tid

Ovenstående har taget 3 timer at udføre