---
hide:
  - footer
---

# Issue 02 - RPi Pico W udviklingsmiljø

## Information

Undersøg hvilket udviklingsmiljø der skal anvendes til at udvikle kode i microPython

## Dokumentation

### Hvilket udviklingsmiljø er bedst til udvikling på rpi pico w ?

[Thonny](https://thonny.org/) er den oficielle editor til rpi pico og er nemt at opsætte, der er dog en hel del mangler i deres editor ifht. vs code. 
[Platform.io](https://docs.platformio.org) er en udvidelse til vs code men supporterer pt. ikke micropython.

Indtil videre bruges Thonny da det er hurtigt at nemt at komme i gang med og vurderes pt. som fyldestgørende til et POC.  

### Find dokumentation

- Raspberry pi pico python SDK dokumentation [https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf)

### Opsætning af udviklingsmiljø

- Opsætning af [Platform.io](https://docs.platformio.org) i vs code kan findes her [https://docs.platformio.org/en/stable/integration/ide/vscode.html#ide-vscode](https://docs.platformio.org/en/stable/integration/ide/vscode.html#ide-vscode)

- [Thonny](https://thonny.org/) er en enkelt installations fil

- En ny Pico skal flashes med enten [microPython](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html#what-is-micropython) eller [C/C++ SDK](https://www.raspberrypi.com/documentation/microcontrollers/c_sdk.html#raspberry-pi-pico-ccpp-sdk) 

- Ikke officiel windows C/C++ installer [https://github.com/ndabas/pico-setup-windows](https://github.com/ndabas/pico-setup-windows)

### Hello world

1. I Thonny lav en fil med koden 
  ```Py
  print('hello RPi Pico')
  ```
2. Kør filen med F5
3. Bekræft at output viser 
```py
>>> %Run -c $EDITOR_CONTENT
hello RPi Pico
>>> 
```

### Blink onboard led

```py
# library import
import machine
import utime

# global variables
led_onboard = machine.Pin("LED", machine.Pin.OUT)

# main program
while True:
    led_onboard.value(1)
    utime.sleep(1)
    led_onboard.value(0)
    utime.sleep(1)
```

## Dokumentation af opsætning

_MicroPython_

1. Flash en Pico W med [drag and drop microPython](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html#drag-and-drop-micropython)
2. Prøv eksempler fra [https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-python-sdk.pdf)

_Arduino_

## Links

- [https://picockpit.com/raspberry-pi/everything-about-the-raspberry-pi-pico-w/](https://picockpit.com/raspberry-pi/everything-about-the-raspberry-pi-pico-w/)