---
hide:
  - footer
---

# Issue 04 - Lav system use cases

## Information

Undersøg og dokumenter hvilke use cases der findes for systemet

## Dokumentation

### Beskriv use cases med step-by-step guides
### Lav flow diagram for hver use case som kan bruges til at implementere kode
### Dokumentér

## Links

