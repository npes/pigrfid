---
hide:
  - footer
---

# Issue x - opgave titel

## Information

## Dokumentation

``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  B ---->|No| E[Sus!];
```

**MD5**  
$F(B,C,D) = (B \land C) \lor (\lnot B \land D )$  
$G(B,C,D) = (B \land D) \lor (C \land \lnot D )$  
$H(B,C,D) = B \oplus C \oplus D$  
$I(B,C,D) = C \oplus (B \lor \lnot D)$  

## Links

