---
title: 'uhf rfid readers'
main_author: 'Nikolaj Simonsen'
hide:
  - footer
---

# UHF RFID læsere research

Det er overordnet svært at finde læsere der tilbyder lang rækkevidde.  
Andreas Spiess har lavet en video om emnet [https://youtu.be/xaLSkyGTh2k](https://youtu.be/xaLSkyGTh2k)  

Konklusionen er at rækkevidde afhænger af modtagerens antenne gain (som er svær at ændre på eksisterende hylde produkter), det anvendte tag (som ikke kan ændres) og omgivelserne (vægge, indstråling, retning osv.).    

Læseren der skal bruges i dette projekt skal kunne interfaces til en microprocessor.  

## Anvendte standarder

- EPCglobal UHF Class 1 Generation 2 [https://www.gs1.org/standards/rfid/uhf-air-interface-protocol](https://www.gs1.org/standards/rfid/uhf-air-interface-protocol)
- ISO/IEC 18000-6 C [https://www.iso.org/standard/59644.html](https://www.iso.org/standard/59644.html)


## Tidligere anvendte læsere

### Sick RFU620-10100

| Egenskaber      |                                                       |
| --------------- | ----------------------------------------------------- |
| Version         | Mid Range                                             |
| Produktkategori | RFID-skrive-/læseenhed med integreret antenne         |
| Frekvensbånd    | UHF (860 MHz ... 960 MHz)                             |
| Bærefrekvens    | 865,7 MHz ... 867,5 MHz                               |
| Udgangseffekt   | 0,25 W (ERP, 24 dBm)                                  |
| RFID-standard   | EPCglobal UHF Class 1 Generation 2, ISO/IEC 18000-6 C |
| Modulation      | PR-ASK                                                |

- datablad net - [https://cdn.sick.com/media/pdf/2/52/052/dataSheet_RFU620-10100_1062599_da.pdf](https://cdn.sick.com/media/pdf/2/52/052/dataSheet_RFU620-10100_1062599_da.pdf)
- datablad lokalt [files/dataSheet_RFU620-10100_1062599_da.pdf](files/dataSheet_RFU620-10100_1062599_da.pdf)

### FM-503 (Fonkan)

- Produkt side [http://www.fonkan.com/en_product_view_747.html](http://www.fonkan.com/en_product_view_747.html)
- Python eksempel [https://github.com/Beastmazter/Mybasement/blob/main/FM503comm.py](https://github.com/Beastmazter/Mybasement/blob/main/FM503comm.py)
- Bruger så vidt jeg kan se en FI-R300T UHF Reader [datablad](files/FI-R300T%20UHF%20Reader%20Module%20datasheet-20170824.pdf)

Det er tæt på umuigt at finde noget dokumentation på protokollen til at kommunikere med enheden, det er UART ved jeg men så ved jeg heller ikke meget mere

## Mulige læsere

Yanzeo laver nogle mulige kandidater der koster fra ca. 200 euro og op [https://www.yanzeo.com/rfid-write-reader/fixed-rfid-readers.html](https://www.yanzeo.com/rfid-write-reader/fixed-rfid-readers.html) 

### Yanzeo

- R784 UHF RFID Reader 6m Long Range RJ45 USB RS232/RS485/Wiegand Output Outdoor IP67 9dbi Antenna Integrated UHF Reader [https://www.yanzeo.com/rfid-write-reader/fixed-rfid-readers/r784-uhf-rfid-reader-6m-long-range-rj45-usb-rs232-rs485-wiegand-output-outdoor-ip67-9dbi-antenna-integrated-uhf-reader.html](https://www.yanzeo.com/rfid-write-reader/fixed-rfid-readers/r784-uhf-rfid-reader-6m-long-range-rj45-usb-rs232-rs485-wiegand-output-outdoor-ip67-9dbi-antenna-integrated-uhf-reader.html)
    
    - 6M Long Range UHF Integrated Reader.
    - High Performance Yanzeo RFID Chip.
    - TCP/IP RS232/RS485/Wiegand USB
    - Waterproof, Durable ,Rugged UHF RFID Reader.
    - Products are manufactured in ISO certified factories. 
    - 220 euro `https://www.amazon.de/-/en/Yanzeo-Reader-Wiegand-Output-Antenna/dp/B08HMT3CJS/ref=sr_1_1?crid=2R560LUXC2136&keywords=yanzeo+R784&qid=1670929349&sprefix=yanzeo+r784%2Caps%2C115&sr=8-1`

- R785 UHF RFID Reader 12m Long Range Outdoor IP67 10dbi Antenna USB RS232/RS485/Wiegand Output UHF Integrated Reader [https://www.yanzeo.com/rfid-write-reader/fixed-rfid-readers/r785-uhf-rfid-reader-12m-long-range-outdoor-ip67-10dbi-antenna-usb-rs232-rs485-wiegand-output-uhf-integrated-reader.html](https://www.yanzeo.com/rfid-write-reader/fixed-rfid-readers/r785-uhf-rfid-reader-12m-long-range-outdoor-ip67-10dbi-antenna-usb-rs232-rs485-wiegand-output-uhf-integrated-reader.html)

    - Support ISO18000-6C/EPC Gen2 ,ISO18000-6B.
    - 12M Long Range UHF Integrated Reader.
    - Products are manufactured in ISO certified factories.
    - Waterproof, Durable ,Rugged UHF RFID Reader
    - 309 euro `https://www.amazon.de/-/en/Yanzeo-Outdoor-Antenna-Wiegand-Integrated/dp/B08HMRRM12/ref=sr_1_1?crid=35XBTTNDLK9CH&keywords=yanzeo+R785&qid=1670930320&sprefix=yanzeo+r785%2Caps%2C197&sr=8-1`