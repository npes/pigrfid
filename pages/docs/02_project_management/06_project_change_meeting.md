---
title: 'Svine RFID'
main_author: 'Nikolaj Simonsen'
hide:
  - footer
---

# Møde den 18. August 2023 med lklm

60 minutter

## Agenda 

1. Ændring af scope på grund af ændringer i andre projekter
2. Hvad kan give værdi at jeg gør herfra
3. Hvad skal output være

## 1 - Ændring af scope på grund af ændringer i andre projekter

Der er sket ændringer i de to projekter som sist og simh har/skulle lave og dermed er der ikke basis for at forfølge oprindeligt scope med at udvikle system og opsamle data i stalden, det er en stor ændring og derfor mødet.

## 2 - Hvad kan give værdi at jeg gør herfra

Det kan give værdi at systematisk evaluere de tags vi har fra grisestalden i forhold til afstand og læsevinkel.  
Eksperimentet skal være reproducerbart.   
Lav en dokumenteret opstilling hvor tags kan læses, opstilling skal kunne måle læseafstand med tags i forskellige vinkler.  

## 3 - Hvad skal output være

Kontakt de virksomheder der er indblandet i projektet og lav formidling af det evaluerede.  
Måske en anbefaling af andre typer tags indenfor iso18006-c som kunne være mere brugbare. 
Beskriv mulige use cases som automatiseret læsning kan bruges til. 