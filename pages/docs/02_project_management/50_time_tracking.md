---
title: 'Svine RFID'
main_author: 'Nikolaj Simonsen'
hide:
  - footer
---

# Tidsforbrug i projektet

| Dato     | Opgavebeskrivelse                    | tidsforbrug |
| -------- | ------------------------------------ | ----------- |
| 20221028 | Forståelse af projekt m. LKLM        | 1 time      |
| 20221031 | Møde hos Danish Pig genetics P/S     | 3,5 time    |
| 20221122 | Indkøb af raspberry pi               | 1 time      |
| 20221201 | Beskrivelse af projektide            | 1,5 time    |
| 20221201 | Oprettelse af gitlab projekt         | 1 time      |
| 20221201 | Blokdiagram                          | 0,5 time    |
| 20221201 | Opsætning af udviklingsmiljø         | 2 time      |
| 20221201 | Test af wifi på pico W               | 0,5 time    |
| 20221201 | Dokumentation                        | 1 time      |
| 20221209 | Start udvikling af API til test      | 4 timer     |
| 20221213 | Research af RFID læsere              | 5 timer     |
| 20230124 | FM-503 rfid reader test              | 3 timer     |
| 20230124 | RPi Pico W FM-503 UART communication | 6 timer     |
|          | total                                | 30 timer    |
