---
title: 'Svine RFID'
main_author: 'Nikolaj Simonsen'
hide:
  - footer
---

# Møde den 06. Marts 2023 med sist og mgre

 30 minutter

## Agenda 

1.	Hvor skal data afleveres (storage, db ?)
    - jeg holder mig fra rpi
    - Find selv ud af det
2.	Hvordan kan video og rfid data synkroniseres.
    - Tidsstemplet med dags datostempel fra rpi
    - Der synkes ikke til en ntp server
    - Antag at der i fremtiden bruges ntp tid på rpi
    - time.time
    - case1_cam1_20-10-22_09_40_50
3.	Hvordan jeg kan koble op til wifi i stalden.
    - umiddelbart er der ikke wifi i stalden på nuværende system.
    - kan secomea lave et wifi hotspot?
    - der er en switch koblet på secomea med poe som der kan kobles et hotspot på.

Målet for det overordnede projekt er at finde ud af hvad data kan bruges til af nye ting, der findes allerede systemer der kan tracke grise i en grisestald.
