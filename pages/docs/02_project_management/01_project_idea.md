---
title: 'Svine RFID'
main_author: 'Nikolaj Simonsen'
hide:
  - footer
---

# Svine RFID – udvikling af “Proof Of Concept” for trådløs RFID modtager 

## Projektansvarlig 

Nikolaj Simonsen 

## Navne på projektmedarbejdere 

Nikolaj Simonsen 

## Baggrund og vision  

Baggrunden for projektet er et ønske fra svineavls industrien om at kunne opnå større indsigt i grises adfærd i svinestalden ved hjælp af automatiseret registrering af hvornår grisene spiser, drikker og hviler. 

Avlsgrise er udstyret med et radiofrekvensidentifikations (RFID) øremærke som kan aflæses med en RFID modtager. 

I dag udføres aflæsning af grisenes øremærker manuelt af personalet i stalden, ved hjælp af håndholdte modtagere. Aflæsninger foretages ikke ofte og slet ikke i forbindelse med grisenes adfærds aktiviteter. 

Projektet er en del af et større projekt om grises adfærd og dette delprojekt er således et udviklingsprojekt der vil forsøge at udvikle et “Proof Of Concept” (POC) for en RFID modtager der kan placeres i grisestien ved for eksempel foder og vand automater. 

Modtageren skal kontinuerligt registrere grisenes RFID øremærke når de er i nærheden af modtageren. Modtageren sender trådløst registreringerne videre til en central computer/database. 
 
RFID øremærkerne benytter standarden ISO18000-6c og udfordringen er blandt andet at rækkevidden mellem modtager og sender er forholdsvis kort (30 cm) hvilket giver nogle begrænsninger i forhold til placering af modtager samt hvor konsistent læsninger kan foretages. 

Projektet vil tilstræbe at benytte, i forhold til industrikomponenter, billige hardware komponenter.  Projektets problemstilling kan sidestilles med en typisk Internet Of Things problematik hvor store og dyre sensorer der manuelt aflæses med lav frekvens kan erstattes af små billige sensorer der kontinuerligt aflæses.   

## Hvad er projektets gevinster ?

1. Et hardware POC for en RFID modtager 

2. Viden om muligheder og begrænsninger for RFID modtageren 
    - Rækkevidde 
    - Pris 
    - Batteri levetid 
    - Placering i grisestalden 
    - Andre anvendelses muligheder 

3. Formidling af opnået viden til interessenter fra industrien  
4. Formidling af udviklingsforløbet til uddannelser  

 
## Foreløbig tidsplan og tidspunkt for forventede gevinster 

1. Start (måned/år): 01/2023 
2. Slut (måned/år): 12/2023 

## Forventede gevinster (gerne med forventet tidspunkt)

- 03/2023 - POC udviklet 
- 05/2023 - Test af POC fuldført 
- 09/2023 - Formidling af opnået viden 
- 09/2023 - Formidling af udviklingsforløb  
 
## Forventede udgifter  

(Forventede udgifter til produkter, materialer, transport, konferencer mv. der er nødvendig for projektets gennemførelse. Ressourceallokering skal ikke medtages.) 

For nuværende er det rimelig ukendt men en forhåbning er at kunne holde de totale udviklingsomkostninger (indkøb af hardware) for en POC under 10000 dkk.  
I udviklingsomkostninger er udgifter til transport etc. Ikke medtaget. 

